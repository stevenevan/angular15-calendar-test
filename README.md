## Requirements

- [x] Display 'open' job requests available for assignment to technicians.

- [x] Show 'assigned' job requests for each technician.

- [x] Allow dragging of 'open' job requests to a technician.

- [x] Enable adjusting the time on the Gantt chart.

- [x] Reassign 'assigned' job requests to other technicians.

- [x] Unassign 'assigned' job requests.

- [ ] Handle timezones.
