import { ChangeDetectionStrategy, Component } from '@angular/core';
import {
  CalendarEvent,
  CalendarEventTimesChangedEvent,
} from 'angular-calendar';
import { addHours, addMinutes, isSameDay, startOfDay } from 'date-fns';
import { Subject } from 'rxjs';
import { colors } from './colors';
import { User } from './day-view-scheduler/day-view-scheduler.component';

const users: User[] = [
  {
    id: 0,
    name: 'Steven Evan',
    color: colors.yellow,
  },
  {
    id: 1,
    name: 'Bagas Muksin',
    color: colors.blue,
  },
  {
    id: 2,
    name: 'Byron ',
    color: colors.green,
  },
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  viewDate = new Date();

  users = users;

  events: CalendarEvent[] = [
    {
      id: 1,
      title: 'An event',
      color: users[0].color,
      start: addHours(startOfDay(new Date()), 5),
      meta: {
        user: users[0],
      },
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      draggable: true,
    },
    {
      id: 2,
      title: 'Another event',
      color: users[1].color,
      start: addHours(startOfDay(new Date()), 2),
      meta: {
        user: users[1],
      },
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      draggable: true,
    },
    {
      id: 3,
      title: 'A 3rd event',
      color: users[0].color,
      start: addHours(startOfDay(new Date()), 7),
      meta: {
        user: users[0],
      },
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      draggable: true,
    },
    {
      id: 4,
      title: 'An all day event',
      color: users[2].color,
      start: addHours(startOfDay(new Date()), 7),
      meta: {
        user: users[2],
      },
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      draggable: true,
    },
    {
      id: 5,
      title: 'Another all day event',
      color: users[2].color,
      start: addHours(startOfDay(new Date()), 9),
      meta: {
        user: users[2],
      },
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      draggable: true,
    },
    {
      id: 6,
      title: 'A 3rd all day event',
      color: users[0].color,
      start: addHours(startOfDay(new Date()), 9),
      meta: {
        user: users[0],
      },
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      draggable: true,
    },
  ].map((event: any) =>
    event.end ? event : { ...event, end: addMinutes(event.start, 30) }
  );

  externalEvents: CalendarEvent[] = [
    {
      id: 7,
      title: 'New Job 1',
      color: colors.yellow,
      start: new Date(),
      draggable: true,
    },
    {
      id: 8,
      title: 'New Job 2',
      color: colors.blue,
      start: new Date(),
      draggable: true,
    },
    {
      id: 9,
      title: 'New Job 3',
      color: colors.blue,
      start: new Date(),
      draggable: true,
    },
    {
      id: 10,
      title: 'New Job 4',
      color: colors.blue,
      start: new Date(),
      draggable: true,
    },
    {
      id: 11,
      title: 'New Job 5',
      color: colors.blue,
      start: new Date(),
      draggable: true,
    },
  ];
  refresh = new Subject<void>();

  eventTimesChanged(
    eventTimesChangedEvent: CalendarEventTimesChangedEvent
  ): void {
    console.log('eventTimesChanged');
    delete eventTimesChangedEvent.event.cssClass;
    const { event, newStart, newEnd, type } = eventTimesChangedEvent;
    //     delete event.cssClass;
    const overlappedEvents = this.getOverlappedEvents(eventTimesChangedEvent);
    console.log({ overlappedEvents, type, event });
    if (overlappedEvents.length > 0) {
      const sameUserOverlappedEvents = overlappedEvents.filter(
        (ev) => ev.meta.user?.id === event.meta.user?.id
      );
      if (sameUserOverlappedEvents.length > 0) {
        event.meta.isOverlapping = true;
        this.events = [...this.events];
        this.refresh.next();
        return;
      }
    }
    const prevEvent = {
      ...event,
      meta: { ...event.meta, prevEvent: undefined },
    };
    event.start = newStart;
    if (newEnd) {
      event.end = newEnd;
    } else if (newStart) {
      event.end = addMinutes(newStart, 30);
    }
    event.meta.prevEvent = prevEvent;
    if (typeof event.meta?.columnIndex === 'number') {
      this.eventDropped(eventTimesChangedEvent);
      this.refresh.next();
      return;
    }
    this.events = [...this.events];
    this.refresh.next();
  }

  userChanged({
    event,
    newUser,
    prevUser,
  }: {
    event: CalendarEvent<any>;
    newUser: User;
    prevUser: User;
  }): void {
    console.log('userChanged');
    if (event.meta.isOverlapping) {
      delete event.meta.isOverlapping;
      this.events = [...this.events];
      return;
    }
    const overlappedEvents = this.getOverlappedEvents({
      event,
      newStart: event.start,
      newEnd: event.end,
    });
    console.log({ event, newUser, overlappedEvents, events: this.events });
    if (overlappedEvents.length > 0) {
      const sameWithNewUser = overlappedEvents.filter(
        (ev) => ev.meta.user?.id === newUser.id
      );
      if (sameWithNewUser.length > 0) {
        if (event.meta.prevEvent) {
          event.start = event.meta.prevEvent?.start;
          event.end = event.meta.prevEvent?.end;
          event.meta.prevEvent = undefined;
          this.events = [...this.events];
          this.refresh.next();
        }
        return;
      }
    }
    event.color = newUser.color;
    event.meta.user = newUser;
    this.events = [...this.events];
  }

  externalDrop(event: CalendarEvent) {
    console.log('externalDrop');
    if (this.externalEvents.indexOf(event) === -1) {
      this.events = this.events.filter((iEvent) => iEvent !== event);
      this.externalEvents.push({ ...event, meta: undefined });
    }
  }

  eventDropped({ event }: CalendarEventTimesChangedEvent): void {
    console.log('eventDropped', { event });
    const externalIndex = this.externalEvents.indexOf(event);
    console.log('eventDropped', { externalIndex, event });
    if (externalIndex > -1) {
      const overlappedEvents = this.getOverlappedEvents({
        event,
        newStart: event.start,
        newEnd: event.end,
      });
      console.log('eventDropped', { overlappedEvents, externalIndex, event });
      if (overlappedEvents.length > 0) {
        const targetUser = this.users[event.meta.columnIndex];
        const sameWithTargetUser = overlappedEvents.filter(
          (ev) => ev.meta.user?.id === targetUser.id
        );
        if (sameWithTargetUser.length > 0) {
          return;
        }
      }
      if (typeof event.meta?.columnIndex === 'number') {
        event.meta = {
          user: users[event.meta.columnIndex],
        };
        event.resizable = {
          beforeStart: true,
          afterEnd: true,
        };
      }
      this.externalEvents.splice(externalIndex, 1);
      if (!event.end && event.start) {
        event.end = addMinutes(event.start, 30);
      }
      this.events.push(event);
    }
    this.events = [...this.events];
    this.refresh.next();
  }

  getOverlappedEvents = ({
    event,
    newStart,
    newEnd,
  }: Pick<CalendarEventTimesChangedEvent, 'event' | 'newStart' | 'newEnd'>) => {
    delete event.cssClass;

    // don't allow dragging or resizing events to different days

    const sameDay = isSameDay(newStart, newEnd!);

    if (!sameDay) {
      return [];
    }
    // don't allow dragging events to the same times as other events
    const overlappingEvents = this.events.filter((otherEvent) => {
      if (otherEvent.id === event.id) return false;
      if (otherEvent.allDay) return false;
      console.log({
        otherEvent,
        event,
        cod1: otherEvent.meta.user?.id === event.meta.user?.id,
        cod2: otherEvent.start < newStart && newStart < otherEvent.end!,
        cod3: otherEvent.start < newEnd! && newStart < otherEvent.end!,
      });
      return (
        (otherEvent.start < newStart && newStart < otherEvent.end!) ||
        (otherEvent.start < newEnd! && newStart < otherEvent.end!)
      );
    });

    return overlappingEvents;
  };
}
